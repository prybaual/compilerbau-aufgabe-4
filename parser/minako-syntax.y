%define parse.error verbose
%define parse.trace

%code requires {
	#include <stdio.h>
	
	extern void yyerror(const char*);
	extern FILE *yyin;
}

%code {
	extern int yylex();
	extern int yylineno;
}

%union {
	char *string;
	double floatValue;
	int intValue;
}

%token AND           "&&"
%token OR            "||"
%token EQ            "=="
%token NEQ           "!="
%token LEQ           "<="
%token GEQ           ">="
%token LSS           "<"
%token GRT           ">"
%token KW_BOOLEAN    "bool"
%token KW_DO         "do"
%token KW_ELSE       "else"
%token KW_FLOAT      "float"
%token KW_FOR        "for"
%token KW_IF         "if"
%token KW_INT        "int"
%token KW_PRINTF     "printf"
%token KW_RETURN     "return"
%token KW_VOID       "void"
%token KW_WHILE      "while"
%token CONST_INT     "integer literal"
%token CONST_FLOAT   "float literal"
%token CONST_BOOLEAN "boolean literal"
%token CONST_STRING  "string literal"
%token ID            "identifier"

// definition of association and precedence of operators
%left '+' '-' OR
%left '*' '/' AND
%nonassoc UMINUS

// workaround for handling dangling else
// LOWER_THAN_ELSE stands for a not existing else
%nonassoc LOWER_THAN_ELSE
%nonassoc KW_ELSE

%%

program:
	program_extra
	;

program_extra: 
	// empty
	| program_declass_functiondef program_extra
	;

program_declass_functiondef:
	type id program_declass_functiondef_end;

program_declass_functiondef_end:
	'(' functiondef_rest
	| declassignment_rest
	;

functiondef_rest:
	functiondefinition_opt ')' '{' statementlist '}';
	
declassignment_rest:
	';'
	| '=' assignment ';'
	;

functioncall:
	id '(' functioncall_opt ')'

functioncall_opt:
	//empty
	| assignment fcall_star
	;

fcall_star:
	//empty
	| ',' assignment fcall_star
	;


functiondefinition:
	type id '(' functiondefinition_opt ')' '{' statementlist '}'

functiondefinition_opt:
	//empty
	| parameterlist
	;

parameterlist:
	type id parameterlist_star;

parameterlist_star:
	//empty
	| ',' type id parameterlist_star
	;

statementlist:
	//empty
	| block statementlist
	;

block:
	'{' statementlist '}'
	| statement
	;

statement:
	ifstatement
	| forstatement
	| whilestatement
	| returnstatement ';'
	| dowhilestatement ';'
	| printf ';'
	| declassignment ';'
	| statassignment ';'
	| functioncall ';'
	;

statass_functioncall:
	id statass_functioncall_rest;

statass_functioncall_rest:
	'=' assignment
	| '(' functioncall_rest
	;

functioncall_rest:
	')'
	| assignment functioncall_rest_star ')'
	;

functioncall_rest_star:
	//empty
	|  ',' assignment functioncall_rest_star
	;


statblock:
	'{' statementlist '}'
	| statement
	;

ifstatement:
	KW_IF '(' assignment ')' statblock ifstatement_opt

ifstatement_opt:
	//empty
	| KW_ELSE statblock
	;

forstatement:
	KW_FOR '(' forstatement_or ';' expr ';' statassignment ')' statblock

forstatement_or:
	statassignment
	| declassignment
	;

dowhilestatement:
	KW_DO statblock KW_WHILE '(' assignment ')';

whilestatement:
	KW_WHILE '(' assignment ')' statblock;

returnstatement:
	KW_RETURN returnstatement_opt;

returnstatement_opt:
	//empty
	| assignment
	;

printf:
	KW_PRINTF '(' printfor ')'

printfor:
	assignment
	| CONST_STRING
	;

declassignment:
	type id declassignment_opt;

declassignment_opt:
	//empty
	| '=' assignment
	;

statassignment:
	id '=' assignment;

assignment:
	id '=' assignment
	| expr
	;

expr: 
	simpexpr expr_opt;

expr_opt:
	//empty
	| EQ simpexpr
	| NEQ simpexpr
	| LEQ simpexpr
	| GEQ simpexpr
	| LSS simpexpr
	| GRT simpexpr

simpexpr:
	'-' term simpexpr_star
	| term simpexpr_star
	;

simpexpr_star:
	//empty
	| '+' term simpexpr_star
	| '-' term simpexpr_star
	| OR term simpexpr_star
	;

term:
	factor term_star;

term_star:
	//empty
	| '*' factor term_star
	| '/' factor term_star
	| AND factor term_star
	;

factor:
	 CONST_INT
	| CONST_FLOAT
	| CONST_BOOLEAN
	| id
	| functioncall
	| '(' assignment ')'
	;

id_functioncall:
	id id_functioncall_rest;

id_functioncall_rest:
	//empty
	| '(' functioncall_rest ')'
	;

functioncall_rest:
	//empty
	| assignment functioncall_assignment_star

functioncall_assignment_star:
	//empty
	| ',' assignment functioncall_assignment_star
	;

type: 
	KW_BOOLEAN
	| KW_FLOAT
	| KW_INT
	| KW_VOID
	;

id: ID;

%%

int main(int argc, char *argv[]) {
	yydebug = 1;

	if (argc < 2) {
		yyin = stdin;
	} else {
		yyin = fopen(argv[1], "r");
		if (yyin == 0) {
			printf("ERROR: Datei %s nicht gefunden", argv[1]);
		}
	}

	return yyparse();
}

void yyerror(const char *msg) {
	fprintf(stderr, "Line %d: %s\n", yylineno, msg);
}
